Param( 
  [Parameter(Mandatory=$true,ValueFromPipeline=$true)][string]$PfxToSignWithLocation,
  [Parameter(Mandatory=$true,ValueFromPipeline=$true)][string]$PfxPassword
)
function Get-AbsolutePath ($Path)
{
    # System.IO.Path.Combine has two properties making it necesarry here:
    #   1) correctly deals with situations where $Path (the second term) is an absolute path
    #   2) correctly deals with situations where $Path (the second term) is relative
    # (join-path) commandlet does not have this first property
    $Path = [System.IO.Path]::Combine( ((pwd).Path), ($Path) );

    # this piece strips out any relative path modifiers like '..' and '.'
    $Path = [System.IO.Path]::GetFullPath($Path);

    return $Path;
}
$full_path_of_pfx = Get-AbsolutePath $PfxToSignWithLocation
echo $full_path_of_pfx
& .\mountfix\make-mountfix.ps1 -PFXLOC $full_path_of_pfx -PFXPASS $PfxPassword
& .\installer\buildInstallers.ps1 -PFXLOC $full_path_of_pfx -PFXPASS $PfxPassword
#copy completed installers and mountfix to output folder
mkdir -Path output
pushd output
cp ..\mountfix\bin\mountfix.exe .
mv ..\installer\Output\*.exe .
popd