
Param($PFXLOC,
$pfxpass
)
pushd $PSScriptRoot

function BuildAndSignInstaller ([string]$installtype)
{
    #build installer
    $fulliscccmd = " "+"/F`"$installtype`"" +" "+"/DOCData=$installtype" +" "+ "install_script.iss"
    $executeme = $PSScriptRoot+"\..\tools\InnoTool\ISCC.exe" +$fulliscccmd
    #run code to actually build installer
    cmd.exe /c $executeme | Out-Host
    #sign installer
    ..\tools\signtool.exe sign /a /f `"$PFXLOC`" /t http://timestamp.comodoca.com/authenticode /p `"$pfxpass`" .\Output\$installtype.exe | Out-Host
}

BuildAndSignInstaller "stripped"
BuildAndSignInstaller "nonstripped"

popd