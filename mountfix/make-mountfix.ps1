Param(
$PFXLOC,
$pfxpass
)
function Get-MSBUILD-Exe([string]$dotnetversion)
{
    $regKey = "HKLM:\software\Microsoft\MSBuild\ToolsVersions\$dotnetversion"
    $regProperty = "MSBuildToolsPath"
    $msbuildExe = join-path -path (Get-ItemProperty $regKey).$regProperty -childpath "msbuild.exe"
    $msbuildExe
}
pushd $PSScriptRoot


$filestosign = Get-ChildItem (join-path $PWD "steamdirectoryfinder\Resources\*") -Recurse -Include *.exe, *.dll
foreach($file in $filestosign)
{
..\tools\signtool.exe sign /a /f `"$PFXLOC`" /t http://timestamp.comodoca.com/authenticode /p `"$pfxpass`" $file.FullName
}

if($Env:APPVEYOR -eq $true)
{
    #build project and tests
    nuget restore
    msbuild /t:steamdirectoryfinder:Rebuild /t:mountfix-tests:Rebuild /logger:"C:\Program Files\AppVeyor\BuildAgent\Appveyor.MSBuildLogger.dll"
}
if($Env:APPVEYOR -ne $true)
{
    #build probject
    &(Get-MSBUILD-Exe "4.0").ToString() | Out-Host
    ..\tools\signtool.exe sign /a /f `"$PFXLOC`" /t http://timestamp.comodoca.com/authenticode /p `"$pfxpass`" .\bin\mountfix.exe
}

popd