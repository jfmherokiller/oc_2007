﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using steamdirectoryfinder.Properties;

namespace steamdirectoryfinder.bothServerAndClient
{
    internal static class ClientAndServer
    {
        public static void DeleteVpks(IEnumerable<string> listOfVpksToDelete)
        {
            foreach (var vpk in listOfVpksToDelete.Where(avv => !avv.Contains(@"platform")))
            {
                MiscFunctions.DeleteFile(vpk);
            }
        }

        public static void ExtractResourcesForBoth()
        {
            File.WriteAllBytes("HLExtract.exe", Resources.HLExtract);
            File.WriteAllBytes("HLLib.dll", Resources.HLLib);
            File.WriteAllBytes("7za.exe", Resources._7za);
        }

        public static void Performtasksi(string prog, string ass)
        {
            var task = new Process
            {
                StartInfo =
                {
                    UseShellExecute = true,
                    FileName = prog,
                    Arguments = ass
                }
            };
            task.Start();

            task.WaitForExit();
            task.Close();
        }

        public static IEnumerable<string> Returnallvpks(string dir)
        {
            return Directory.EnumerateFiles(dir, "*.vpk", SearchOption.AllDirectories);
        }

        public static IEnumerable<string> Returndirvpks(string dir)
        {
            var vpkfiles = Directory.EnumerateFiles(dir, "*_dir.vpk", SearchOption.AllDirectories);
            return vpkfiles;
        }
        public static void Runoneachvpk(IEnumerable<string> ass)
        {
            foreach (var avv in ass.Where(avv => !avv.Contains(@"platform")))
            {
                ExtractGameResources(avv);
            }

        }
        public static void Runoneachvpk(IEnumerable<string> ass, string ocinstalldir)
        {
            foreach (var avv in ass.Where(avv => !avv.Contains(@"platform")))
            {
                var gameName = new DirectoryInfo(Path.GetDirectoryName(avv)).Name;
                switch (gameName)
                {
                    //revert to orginal mount code if half life campaign is detected
                    case @"hl2":
                    case @"ep2":
                    case @"episodic":
                    case @"hl1":
                    case @"lostcoast":
                        ExtractGameResources(avv);
                        break;
                        //change cstrike to css in order to correctly specify the addon name
                    case @"cstrike":
                        ExtractGameResourcesToAddonsFolder(avv, @"css", ocinstalldir);
                        break;
                    default:
                        ExtractGameResourcesToAddonsFolder(avv, gameName, ocinstalldir);
                        break;
                }
            }
        }

        public static void Performtasks(string prog, string ass)
        {
            var task = new Process
            {
                StartInfo =
                {
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    FileName = prog,
                    Arguments = ass
                }
            };
            task.Start();
            task.OutputDataReceived += (sender, e) => Console.WriteLine(e.Data);
            task.BeginOutputReadLine();
            task.WaitForExit();
            task.Close();
        }

        private static void ExtractGameResources(string ass)
        {
            var quotedVpk = MiscFunctions.PutIntoQuotes(ass);
            var vpkwithoutextend = ass;
            vpkwithoutextend = vpkwithoutextend.Remove(vpkwithoutextend.IndexOf('.'));
            var gamedir = Path.GetDirectoryName(vpkwithoutextend);
            var robocopyargs = MiscFunctions.PutIntoQuotes(gamedir + "\\root") + " " + MiscFunctions.PutIntoQuotes(gamedir) + "  /E /MOVE /IS  /MT:" +
                               Environment.ProcessorCount;
            var hlExtractargs = "-p " + quotedVpk + " -d " + MiscFunctions.PutIntoQuotes(gamedir) + " " + "-e \"/\"";
            Performtasks("HLExtract.exe", hlExtractargs);
            Performtasks("robocopy", robocopyargs);
        }
        private static void ExtractGameResourcesToAddonsFolder(string ass, string gameName, string ocinstalldir)
        {
            var extractionfolder = ocinstalldir + "\\add-ons\\" + gameName;
            //create addon folder if not exist
            MiscFunctions.CreateDir(extractionfolder);
            var quotedExtractionFolder = MiscFunctions.PutIntoQuotes(extractionfolder);
            var quotedVpk = MiscFunctions.PutIntoQuotes(ass);
            var vpkwithoutextend = ass;
            vpkwithoutextend = vpkwithoutextend.Remove(vpkwithoutextend.IndexOf('.'));
            var gamedir = Path.GetDirectoryName(vpkwithoutextend);
            var robocopyargs = MiscFunctions.PutIntoQuotes(gamedir + "\\root") + " " + quotedExtractionFolder + "  /E /MOVE /IS  /MT:" +
                               Environment.ProcessorCount;
            var hlExtractargs = "-p " + quotedVpk + " -d " + MiscFunctions.PutIntoQuotes(gamedir) + " " + "-e \"/\"";
            Performtasks("HLExtract.exe", hlExtractargs);
            Performtasks("robocopy", robocopyargs);
        }
    }
}
