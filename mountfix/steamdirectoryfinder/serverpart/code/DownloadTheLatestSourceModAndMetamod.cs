using System;
using System.Net;

namespace steamdirectoryfinder.serverpart.code
{
    public static class DownloadTheLatestSourceModAndMetamod
    {
        private const string Sourcemodlink = "http://www.sourcemod.net/downloads.php?branch=stable";
        private const string Metamodlink = "https://www.sourcemm.net/downloads.php?branch=stable";

        private static string DownloadString(string address)
        {
            using (var client = new WebClient())
            {
                return client.DownloadString(address);
            }
        }

        //construct the download links from the pages
        public static Tuple<string, string> DownloadPAges()
        {
            var sourcemodPageData = DownloadString(Sourcemodlink);
            var metamodPageData = DownloadString(Metamodlink);
            //select the fist download link
            var linkbegin = sourcemodPageData.Substring(sourcemodPageData.IndexOf("https://sm.alliedmods.net/smdrop/", StringComparison.Ordinal));
            var sourcemodstring = linkbegin.Substring(0,linkbegin.IndexOf('\''));
            //select the first download link
            linkbegin = metamodPageData.Substring(metamodPageData.IndexOf("https://mms.alliedmods.net/mmsdrop/", StringComparison.Ordinal));
            var metamodstring = linkbegin.Substring(0,linkbegin.IndexOf('\''));

            return new Tuple<string, string>(metamodstring, sourcemodstring);
        }
    }
}